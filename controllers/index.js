'use strict'
const converterExcel = require('excel-as-json').processFile,
    fs = require('fs'),
    async = require('async'),
    path = require('path'),
    firebase = require('firebase'),
    { modelAsset } = require('../models/index'),
    uid = require('uid'),
    chunk = require('lodash.chunk')
require('firebase/firestore')

let options = {
    sheet: '1',
    isColOriented: false,
    omitEmtpyFields: false
}

function excelToJsonStates(req, res) {
    if (req.file) {
        converterExcel(path.join(__dirname, `/../uploads/${req.file.filename}`), undefined, options, (err, data) => {
            let assets = []
            if (!err) {
                modelAsset((asset) => {
                    let documents = (chunk(data, 500))
                    let index = 0
                    async.eachOfLimit(documents, 10, (documentt, key, callback) => {
                        const batch = firebase.firestore().batch()
                        async.forEachOf(documentt, (value, key, callbackk) => {
                            const id = uid(10)
                            const activesRef = firebase.firestore().collection("assets").doc(id)
                            handleAssingData(asset, value, (asset) => {
                                index++
                                console.log('saved: ', index)
                                batch.set(activesRef, asset);
                                callbackk()
                            })
                        }, err => {
                            if (!err) {
                                batch.commit().then(() => {
                                    callback()
                                })
                            }
                        })
                    }, error => {
                        console.log("Termine de guardar en la BD");
                        res.status(200).send({ message: 'Termine de guardar en la BD' })
                    })
                })
            } else {
                console.log('Error al guardar..')
                res.status(500).send({ message: err })
            }
        })
    } else {
        res.status(404).send({ message: 'No se encotro archivo' })
    }
}

function handleAssingData(asset, value, callback) {
    asset.idOwner = value.ID_PROPIETARIO_SATES
    asset.socialReason = value.RAZON_SOCIAL
    asset.creationDate = new Date()
    asset.idSates = uid(10)
    asset.idSates = asset.idSates.toString()
    asset.kindOfGood = value.TIPO_DE_BIEN
    asset.typeOfAsset = value.TIPO_DE_ACTIVO
    asset.title = value.TITULO
    asset.observations = value.OBSERVACIONES
    asset.destination = value.DESTINO
    asset.reviewDays = value.REVISION
    asset.states['idGoverment'] = value.ID_GOBIERNO
    asset.states['basicInformation'][0].id = value.IDENTIFICADOR
    asset.states['basicInformation'][0].valueId = value.VALOR_DEL_IDENTIFICADOR
    asset.states['basicInformation'][0].name = value.NOMBRE_DEL_INMUEBLE
    asset.states['basicInformation'][0].typeService = value.TIPO_DE_SERVICIO
    asset.states['basicInformation'][0].stateService = value.SERVICIO
    asset.states['basicInformation'][0].turn = value.TURNO
    asset.states['basicInformation'][0].director = value.RESPONSABLE
    asset.states['basicInformation'][0]['other'].label = value.OTRO
    asset.states['basicInformation'][0]['other'].value = value.VALOR_DE_OTRO
    asset.states['location'].CP = value.CP
    asset.states['location'].street = value.CALLE
    asset.states['location'].nExterno = value.EXT
    asset.states['location'].nInterno = value.INT
    asset.states['location'].colony = value.COLONIA
    asset.states['location'].typeOfSettlement = value.TIPO_DE_ASENTAMIENTO
    asset.states['location'].municipality = value.MUNICIPIO_DELEGACION
    asset.states['location'].state = value.ESTADO
    asset.states['location'].country = value.PAIS
    asset.states['location'].lat = value.LATITUD
    asset.states['location'].lng = value.LONGITUD
    asset.states['assetData'].superficieTerreno = value.SUPERFICIE_DE_TERRENO
    asset.states['assetData'].superficieConstrucion = value.SUPERFICIE_DE_CONSTRUCCION
    asset.states['assetData'].indiviso = value.INDIVISO
    asset.states['assetData'].antiguedadConstrucion = value.ANTIGUEDAD_DE_CONSTRUCCION
    asset.areas[0].label = value.TIPO_DEL_AREA_1
    asset.areas[0].value = value.NOMBRE_DEL_AREA_1
    asset.areas[1].label = value.TIPO_DEL_AREA_2
    asset.areas[1].value = value.NOMBRE_DEL_AREA_2
    asset.areas[2].label = value.TIPO_DEL_AREA_3
    asset.areas[2].value = value.NOMBRE_DEL_AREA_3
    asset.areas[3].label = value.TIPO_DEL_AREA_4
    asset.areas[3].value = value.NOMBRE_DEL_AREA_4
    asset.areas[4].label = value.TIPO_DEL_AREA_5
    asset.areas[4].value = value.NOMBRE_DEL_AREA_5
    asset.areas[5].label = value.TIPO_DEL_AREA_6
    asset.areas[5].value = value.NOMBRE_DEL_AREA_6
    asset.ids[0].label = value.TIPO_DE_IDENTIFICADOR_1
    asset.ids[0].value = value.VALOR_DEL_IDENTIFICADOR_1
    asset.ids[1].label = value.TIPO_DE_IDENTIFICADOR_2
    asset.ids[1].value = value.VALOR_DEL_IDENTIFICADOR_2
    asset.ids[2].label = value.TIPO_DE_IDENTIFICADOR_3
    asset.ids[2].value = value.VALOR_DEL_IDENTIFICADOR_3
    asset.ids[3].label = value.TIPO_DE_IDENTIFICADOR_4
    asset.ids[3].value = value.VALOR_DEL_IDENTIFICADOR_4
    asset.ids[4].label = value.TIPO_DE_IDENTIFICADOR_5
    asset.ids[4].value = value.VALOR_DEL_IDENTIFICADOR_5
    asset.ids[5].label = value.TIPO_DE_IDENTIFICADOR_6
    asset.ids[5].value = value.VALOR_DEL_IDENTIFICADOR_6
    callback(asset)
}

module.exports = {
    excelToJsonStates
}