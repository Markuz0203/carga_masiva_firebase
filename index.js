const app = require('./app'),
      conf = require('./config'),
      firebase = require('firebase')

      firebase.initializeApp({
          apiKey: "AIzaSyBZn9tGhqNRM6NZZle3AxR_sIAnLERAy6k",
          authDomain: "satespruebas-ab39e.firebaseapp.com",
          databaseURL: "https://satespruebas-ab39e.firebaseio.com",
          projectId: "satespruebas-ab39e",
          storageBucket: "satespruebas-ab39e.appspot.com",
          messagingSenderId: "636263990746"
      })

app.listen(conf.port, () => {
    console.log(`Escuchando en el puerto ${conf.port}`)
})