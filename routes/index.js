'user strict'
const api = require('express').Router(),
      constrollers = require('../controllers/'),
      multer = require('multer'),
      path = require('path'),
      upload = multer({ dest: path.join(__dirname, '/../uploads/') })

api.post('/uploadFile', upload.single('file'), constrollers.excelToJsonStates)

module.exports = api


